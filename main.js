//Load libraries 
var express = require("express");
var path = require("path");

//instance for express
var app = express();

//set the directory name 
app.use(express.static(__dirname+"/public"));
app.use("/libs",express.static(__dirname+"/bower_components"));
app.use("/stylesheet",express.static(__dirname+"/stylesheet"));

app.get("/login",function(req,resp){
    console.log("username: %s, password: %s",req.query.username,req.query.password);
    
    /*var status =0;
    if(req.query.username == req.query.password)
        status = 202;
    else    
        status = 400;
    resp.status(status);*/
    
    var status = (req.query.password == req.query.username)?202:400; 
    resp.status(status);
    if(202 == status){
        //Access the database 
        resp.type("application/json");
        resp.json({
            token:Math.random()
        });
    }
    //resp.status( (req.query.password == req.query.username)?202:400);
    resp.end();
});

//set up the port 
app.set ("port",parseInt(process.argv[2])||3000);

//connect the server 
app.listen(app.get("port"),function(){
console.log("Application is running on Date : %d at Port : %d"
            ,new Date(),app.get("port"));
});

