//start angular  --  IIFE immidetaly invoked function expression 

(function(){

//Create instance of angular app 
var LoginApp = angular.module("LoginApp",[]);


//Define controller function()
var LoginCtrl = function($http,$log){
    var loginCtrl = this;

    loginCtrl.username ="";
    loginCtrl.password ="";

    loginCtrl.performLogin = function(){
        
        $log.info("username: %s, password: %s",loginCtrl.username,loginCtrl.password);
        //prom is promise method()
        var prom = $http.get("/login",{
            params:{
                username:loginCtrl.username,
                password:loginCtrl.password,
                timestamp:(new Date()).toString()
            }
            
        }).then(function(result){
            //console.log("Login sucessfull");
            console.log(JSON.stringify(result.data));
            loginCtrl.status = "Login sucessfull. token: " + JSON.stringify(result.data);

        }).catch(function(status){
            loginCtrl.status = "Username or password are incorrect .Try again";
        });
    }

}

//Create a controller 
//strict dependency injector
LoginApp.controller("LoginCtrl",["$http","$log",LoginCtrl]); 

})();


